INSERT INTO users (email, password, datetime_created) VALUES (" johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES (" juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES (" janesmith@gmail.com", "passwordC", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES (" mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES (" johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


INSERT INTO posts (author_id, title, content, datetime_created) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_created) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_created) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_created) VALUES(4, "Fourth Code", "Bye Bye Solar System!", "2021-01-02 04:00:00");

--GET ID
SELECT * FROM posts WHERE author_id = 1;
--GET ALL
SELECT email, datetime_created FROM users;
--UPDATE
UPDATE posts SET content = "Hello to the people of the Earth" WHERE id = 2;
--DELETE
DELETE FROM users WHERE email = "johndoe@gmail.com";